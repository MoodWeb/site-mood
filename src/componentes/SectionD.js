import React, { Component} from 'react';
import styled from "styled-components";
import { Parallax } from 'react-scroll-parallax';

const ContactContainer = styled.div`
    width:100%;
    background-color:#ffffff;
`;
const Contact = styled.div`
    position:relative;
    width:100%;
`;
const Formulario = styled.form`
    width:50%;
    text-align:center;
    line-height:10vh;
    display:block;
    margin-left:auto;
    margin-right:auto;
    @media (max-width:375px){
        width:100%;
    }
`;
const TitleContact = styled.h1`
    font-size: 13vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #3498DB;
    text-align:center;
    color:#ffffff;
    position:relative;
    margin:0;
`;
const SubTitle = styled.h2`
    position:absolute;
    font-size: 7vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    color:#3498DB;
    margin:0;
    margin-left:45%;
    z-index:100000;
    margin-top:-120px;
    animation-duration: 3s;
    animation-name: contacto;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @media (max-width:375px){
        margin-left:42%;
        margin-top:-10%;
    }
    @media (max-width:414px){
        margin-left:42%;
        margin-top:-10%;
    }
    @keyframes contacto {
        from {
            left: 0;
        } 
        to {
            left: 40px;
        }
`;
const TitleCampo = styled.label`
    font-family:'Titillium Web', sans-serif;
    font-size:20px;
    font-weight:400;
    color:#3498DB;
    margin-left:5px;
`;
const Campo = styled.input`
    position:relative;
    width:33%;
    height:30px;
    margin-left:5px;
    border:none;
    border-bottom:2px solid #7B7D7D ;
    color:#222222;
    font-size:12px;
    box-sizing:border-box;
    outline:0;
    @media (max-width:414px){
        width:95%;
    }
    @media (max-width:375px){
        width:90%;
    }
    &::placeholder{
        color:#3498DB;
        font-size:15px;
        font-family:'Titillium Web', sans-serif;
        font-weight:400;
        opacity:0.5;
    }
    &:focus{
        border-bottom:2px solid #3498DB;
    }
    
`;
const BtnSend = styled.button`
    border: none;
    border-radius:25px;
    width:25%;
    text-align: center;
    cursor: pointer;
    text-transform: uppercase;
    outline: none;
    overflow: hidden;
    position: relative;
    color: #fff;
    font-weight: 700;
    font-family:'Titillium Web', sans-serif;
    font-size: 15px;
    background-color: #000;
    padding: 10px 10px;
    margin: 0 auto;
    
@media (max-width:414px){
        width:90%;
    }
@media (max-width:320px){
    width:75%;
}
  &::after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    height: 490%;
    width: 140%;
    background: #3498DB;
    -webkit-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
    -webkit-transform: translateX(-98%) translateY(-25%) rotate(45deg);
    transform: translateX(-98%) translateY(-25%) rotate(45deg);
  }
  &:hover:after {
    -webkit-transform: translateX(-9%) translateY(-25%) rotate(45deg);
    transform: translateX(-9%) translateY(-25%) rotate(45deg);
  }
`;
const Send = styled.span`
    position:relative;
    z-index:1;
`;
class SectionD extends Component{
    
    nombreRef = React.createRef();
    apellidosRef = React.createRef();
    asuntoRef = React.createRef();
    telefonoRef = React.createRef();
    emailRef = React.createRef();

    enviarDatos = (e) => {
        e.preventDefault();
        const nombre = this.nombreRef.current.value;
        const apellidos = this.apellidosRef.current.value;
        const asunto = this.asuntoRef.current.value;
        const telefono = this.telefonoRef.current.value;
        const email = this.emailRef.current.value;

        alert(`El cliente ${nombre} ${apellidos} quiere que lo contacten para ${asunto}. \n Su telélefono es el ${telefono} y su correo electrónico es ${email}`);
    }
    
    render(){
        return(
            <Parallax>
            <ContactContainer>
                <TitleContact>CONTACTO</TitleContact>
                <SubTitle>CONTACTO</SubTitle>
                <Contact>
                 <Formulario>
                    <Campo type="text" ref={ this.nombreRef } maxLength="20" required placeholder="Nombre"/>
                    <Campo type="text" ref={ this.apellidosRef } maxLength="50" required placeholder="Apellidos"/><br/>
                    <Campo type="text" ref={ this.asuntoRef } maxLength="30" required placeholder="Asunto"/>
                    <Campo type="number" ref={ this.telefonoRef } maxLength="10" required placeholder="Numero a 10 Digitos"/><br/>
                    <Campo type="email"  ref={ this.emailRef } maxLength="30" required placeholder="Correo electronico"/><br/>
                    <BtnSend onClick={ e => this.enviarDatos(e) }><Send>Enviar</Send></BtnSend>
                 </Formulario>
                </Contact>
            </ContactContainer>
            </Parallax>
        );
    }
}


export default SectionD;