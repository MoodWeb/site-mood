import React, { Component} from 'react';
import styled from "styled-components";
import { Parallax } from 'react-scroll-parallax';

const TeamContainer = styled.div`
    width:100%;
    height:auto;
    background-color:#FAFAFA;
    margin:0;
    padding:0;
    @media only screen (min-width:375px){

    }
`;
const TeamTitle = styled.h1`
    font-size: 13vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #330CE9;
    text-align:center;
    color:#FAFAFA;
    position:relative;
    margin:0;
   
  `;
const MemeberContainer = styled.div`
    width:100%;
    display:flex;
    margin-bottom:20px;
    @media (max-width:320px){
        display:block;
        width:95%;
    }
    @media (max-width:375px){
        display:block;
        width:100%;
    }
    @media (max-width:414px){
        display:block;
        width:100%;
    }
`;
const Member = styled.div`
    width:32%;
    position:relative;
    background: linear-gradient(to top, #2e2e2e, #0431B4, #8258FA );
    margin:0 10px 0 10px;
    border-radius:6px;
    cursor:pointer;
    overflow:hidden;
    @media (max-width:375px){
        width:100%;
        margin:auto;
        padding-top:5%;
        margin-bottom:5%;

    }
    @media (max-width:414px){
        width:100%;
        margin:0;
        padding-top:5%;
        margin-bottom:5%;
    }
    @media (max-width:320px){
        width:100%;
        margin:auto;
        margin-bottom:5%;
    }
    &:after{
        background-color: #070719;
        position: absolute;
        content: "";
        top: 20px;
        left: 20px;
        right: 20px;
        bottom: 20px;
        transform: rotate3d(-1, 1, 0, 100deg);
	    transition: all .4s ease-in-out 0s
        opacity: 0;
        cursor:pointer;
        border-radius:6px;
    }
    &:hover{
        transform: translateY(0%);
        opacity: 1;
        transition-delay: 0.3s;
    }
    &:hover:after{
        opacity: .9;
	    transform: rotate3d(0, 0, 0, 0deg)
    }
    
`;

const ImgContainer = styled.div`
    width: 150px;
    height: 150px;
    border-radius: 100%;
    vertical-align: middle;
    margin: auto;
    margin-top:10%;
    @media (max-width:375px){
        width:40%;
        height:40%;
        margin:auto;
        margin-top:5%;
    }
    @media (max-width:414px){
        width:40%;
        height:40%;
        margin:auto;
        margin-top:5%;
    }
`;
const Photo = styled.img`
    width:100%;
    border-radius:100%;
    display:block;
    position:relative;
    margin:auto;
    padding-top: 0%;
    text-align:center;
    @media (max-width:375px){
        width: 100%;
        margin: auto;
        display: block;
        padding-top: 0;
        padding-bottom:0;
    }
    @media (max-width:414px){
        width:100%;
        margin: auto;
        display: block;
        padding-top: 0;
        padding-bottom:0;
    }

`;
const SubTeam = styled.h2`
    position:relative;
    font-size: 7vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    color:#330CE9;
    margin:0;
    margin-left:45%;
    z-index:100000;
    margin-top:-120px;
    animation-duration: 3s;
    animation-name: team;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @media (max-width:375px){
        margin-top:-10%;
    }
    @media (max-width:414px){
        margin-top:-10%;
    }
    @keyframes team {
        from {
            bottom: 0;
        } 
        to {
            bottom: 40px;
        }
`;
const Description = styled.p`
    Color:#f2f3f4;
    font-size:20px;
    text-align:center;
    font-family:'Titillium Web', sans-serif;
    line-height:30px;
    padding:0 90px;
    @media (max-width:375px){
        padding:0 40px;
    }
    @media (max-width:414px){
        padding:0 40px;
    }
`;

class SectionB extends Component{
    render(){
        return(
            <Parallax>
            <TeamContainer>
                <TeamTitle>TEAM</TeamTitle>
                <SubTeam>TEAM</SubTeam>
                <MemeberContainer>
                    <Member>
                        <ImgContainer>
                            <Photo src="../img/jorge.jpeg"/>
                        </ImgContainer>
                        <Description>
                            Jorge I. Fierros
                        </Description>
                    </Member>
                    <Member>
                        <ImgContainer>
                            <Photo src="../img/panda.jpeg"/>
                        </ImgContainer>
                        <Description>
                            Andrés Segura
                        </Description>
                    </Member>
                    <Member>
                        <ImgContainer>
                            <Photo src="../img/daniel.jpg"/>
                        </ImgContainer>
                        <Description>
                            Daniel Arenas
                        </Description>
                    </Member>
                    <Member>
                        <ImgContainer>
                            <Photo src="../img/Recurso_5.svg"/>
                        </ImgContainer>
                        <Description>
                            Valeria Vielmann
                        </Description>
                    </Member>
                </MemeberContainer>
            </TeamContainer>
            </Parallax>
        )
    }
}

export default SectionB;