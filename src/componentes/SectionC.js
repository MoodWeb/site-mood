import React, { Component} from 'react';
import Slidr from './Slider/Slider';
import styled from "styled-components";
import { Parallax } from 'react-scroll-parallax';

const ServicesContainer = styled.div`
    width:100%;
    height: auto;
    display:block;
    @media (max-width:375px){
        display:block;
    }
    @media (max-width:414px){
        display:block;
    }
`;
const ServiceLeft = styled.div`
    width:100%;
    background-color:#FAFAFA;
    @media (max-width:375px){
        width:100%
    }
    @media (max-width:414px){
        width:100%
    }
`;
const ServiceRight = styled.div`
    width:100%;
    background-color:#FAFAFA;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%;
    }
`;
const ServiceTitle = styled.h1`
    position:relative;
    color:#FAFAFA;
    text-shadow: 0 0 4px #3498DB;
    font-size: 13vw;
    margin:0;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-align:center;
    @media (max-width:375px){
        margin:0;
        font-size:13vw;
    }
    @media (max-width:414px){
        margin:0;
        font-size:13vw;
    }
`;
const SubService = styled.h3`
    position:absolute;
    font-size: 7vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    color:#3498DB;
    margin:0;
    margin-left:45%;
    z-index:100000;
    margin-top:-120px;
    animation-duration: 3s;
    animation-name: air;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @media (max-width:375px){
        margin-top:-10%;
        margin-left:42%;
    }
    @media (max-width:414px){
        margin-top:-10%;
        margin-left:42%;
    }
    @keyframes air {
        from {
            left: 0;
        } 
        to {
            left: 40px;
        }
`;
const Breakdown = styled.p`
    width:100%;
    font-family: 'Titillium Web', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: right;
    color:#3498DB;
    line-height:2.9vw;   
`;
class SectionC extends Component{
    render(){
        return(
            <Parallax>
                <ServicesContainer>
                    <ServiceLeft>
                        <ServiceTitle>SERVICIOS</ServiceTitle>
                        <SubService>SERVICIOS</SubService>
                    </ServiceLeft>
                    <ServiceRight>
                        <Slidr/>
                    </ServiceRight>
                </ServicesContainer>
            </Parallax>
        )
    }
}

export default SectionC;