import React, { Component} from 'react';
import styled from "styled-components";
import { Parallax } from 'react-scroll-parallax';

const AboutContainer = styled.div`
    width:100%;
    height: auto;
    display:flex;
    @media (max-width:375px){
        display:block;
    }
    @media(max-width:320px){
        display:block;
    }
    @media (max-width:414px){
        display:block;
    }
`;
const AboutLeft = styled.div`
    width:50%;
    background-color:#ffffff;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:320px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%
    }
`;
const AboutRight = styled.div`
    width:50%;
    background-color:#ffffff;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:320px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%;
    }
`;
const AboutTitle = styled.h1`
    position:relative;
    text-shadow: 0 0 4px #04059B;
    color:#ffffff;
    font-size: 13vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-align:center;
    text-align:center;
    margin:0;
    @media (max-width:375px){
        margin:0;
    }
    @media (max-width:320px){
        margin:0;
    }
    @media (max-width:414px){
        margin:0;
    }
    
`;
const Who = styled.h2`
    font-family:'Montserrat', sans-serif;
    font-size:2.5vw;
    font-weight:700;
    text-align:center;
    color:#330CE9;
    @media (max-width:375px){
        margin:0;
    }
    @media (max-width:320px){
        margin:0;
    }
    @media (max-width:414px){
        margin:0;
    }
`;  
const SubAbout = styled.h3`
    position:relative;
    font-size: 7vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    color:#330CE9;
    margin:0;
    margin-left:30%;
    z-index:100000;
    margin-top:-120px;
    animation-duration: 3s;
    animation-name: title;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @media (max-width:375px){
        margin-left:42%;
        margin-top:-10%;
    }
    @media (max-width:320px){
        margin-left:42%;
        margin-top:-10%;
    }
    @media (max-width:414px){
        margin-left:42%;
        margin-top:-10%;
    }
    @keyframes title {
        from {
            top: 0;
        } 
        to {
            top: 40px;
        }
`;

const Text = styled.p`
    width:90%;
    font-family: 'Titillium Web', sans-serif;
    font-size:1.3vw;
    font-weight:400;
    text-align: right;
    color:#424949;
    line-height:2.9vw;
    margin:5% 5%;
    @media (max-width:375px){
        margin:0 0 0 0%;
        text-align:center;
        font-size:2vw;
    }
    @media (max-width:320px){
        margin:0 0 0 0%;
        text-align:center;
        font-size:2vw;
    }
    @media (max-width:414px){
        margin:0 0 0 5%;
        text-align:center;
        font-size:2vw;
    }

`;
class SectionA extends Component {
    render(){
        return(
            <Parallax>
            <AboutContainer>
                <AboutLeft>
                    <AboutTitle>ABOUT</AboutTitle>
                    <SubAbout>ABOUT</SubAbout>
                </AboutLeft>
                <AboutRight>
                    <Text>
                        <Who>¿Quienes somos?</Who>
                        Mood Web<br/>
                        Somos un equipo de trabajo que quiere hacer equipo contigo y con tu marca, Mood Web nace de la necesidad de una agencia honesta y transparente; en Mood Web te diremos de manera sencilla lo que tu marca necesita para emprender en el mundo digital, sin letras chiquitas, sin costosas campañas que no te servirán.

                        Si buscas soluciones en programación web, redes sociales y comunicación, somos tu mejor opción.
                    </Text>
                </AboutRight>
            </AboutContainer>
            </Parallax>
        )
    }
}






export default SectionA;