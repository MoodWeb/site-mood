import React from 'react';
import SectionA from './SectionA';
import SectionB from './SectionB';
import SectionC from './SectionC';
import SectionD from './SectionD';
import { ParallaxProvider } from 'react-scroll-parallax';

function App() {
  return (
    <ParallaxProvider>
      <SectionA/>
      <SectionB/>
      <SectionC/>
      <SectionD/>
    </ParallaxProvider>
  );
}

export default App;
