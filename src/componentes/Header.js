import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import styled from "styled-components";

const NavContainer = styled.div`
    width: 100%;
    margin:0;
    padding:0;
`;
const NavBar =styled.nav`
    background-color:#FAFAFA;
    width:100%;
    height:70px;
    display:flex;
    justify-content:center;
`;
const BarMenu = styled.ul`
    list-style-type:none;
    margin:0;
    padding:0 0 0 50%;
    display:flex;
    justify-content:center;
    align-items:center;
    font-family: 'Titillium Web', sans-serif;
    font-size:15px;
    @media (max-width:375px){
        padding:0 0 0 7%;
    }
    @media (max-width:320px){
        padding:0 0 0 1%;
    }
    @media (max-width:414px){
        padding:0 0 0 7%;
    }
`;
const Sections = styled.li`
    display:inline-block;
    justify-content:center;
    aling-content:center;
    margin-right:10px;
`;
const Links = styled.a`
    text-decoration:none;
    color:#5EC5E8;
    &:hover{
        color:#04059B;
    }
`;
const Logo = styled.img`
    position:relative;
    width:200px;
    padding:10px 0 10px 0;
    @media (max-width:375px){
        width:30vw;
        padding:20px 0 20px 0;
    }
    @media (max-width:414px){
        width:30vw;
        padding:20px 0 20px 0;
    }
`;
class Header extends Component{
    render() {
        return (
            <NavContainer>
                <NavBar>
                    <Link to="/"><Logo src="../img/MOOD_WEB_2.png"/></Link>
                    <BarMenu>
					<Link to="/about"><Sections><Links>About</Links></Sections></Link>
					<Link to="/team"><Sections><Links>Team</Links></Sections></Link>
					<Link to="/services"><Sections><Links>Servicios</Links></Sections></Link>
					<Link to="/contact"><Sections><Links>Contacto</Links></Sections></Link>
				    </BarMenu>
                </NavBar>
            </NavContainer>
        );
    }
}

export default Header;