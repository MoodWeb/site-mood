import React from 'react';
import styled from 'styled-components';

const HostingContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;
const HostingTitle = styled.h1`
    font-size: 5vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #3498DB;
    text-align:center;
    color:#ffffff;
`;
const DescriptionHosting = styled.div`
    width:100%;
    height:auto;
    margin:0;
    padding:0;
`;
const HostingText = styled.p`
    width:50%;
    height:auto;
    margin:auto;
    font-family: 'Titillium Web', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: center;
    color:#424949;
    line-height:2.9vw;
`;
const SubTitle = styled.span`
    font-family: 'Montserrat', sans-serif;
    font-size: 2.5vw;
    font-weight:700;
    color:#330CE9;
`;
const Hosting = () => {
    return(
        <React.Fragment>
            <HostingContainer>
                <HostingTitle>Hosting</HostingTitle>
                <DescriptionHosting>
                    <HostingText>
                    Te ayudamos a hospedar tu sitio en la nube y al mismo tiempo lo mantenemos actualizado, 
                    te brindamos la mejor oferta en soporte en cualquier momento.<br/>
                    <SubTitle>¡Tu sitio estara seguro y funcionando!</SubTitle>
                    </HostingText>  
                </DescriptionHosting>
            </HostingContainer>
        </React.Fragment>
    );
} 

export default Hosting;