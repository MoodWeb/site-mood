import React from 'react';
import styled from 'styled-components';

const TestContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;
const TestTitle = styled.h1`
    font-size: 5vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #3498DB;
    text-align:center;
    color:#ffffff;
`;
const DescriptionTest = styled.div`
    width:100%;
    height:auto;
    margin:0;
    padding:0;
`;
const TestText = styled.p`
    width:50%;
    height:auto;
    margin:auto;
    font-family: 'Titillium Web', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: center;
    color:#424949;
    line-height:2.9vw;
`;
const SubTitle = styled.span`
    font-family: 'Montserrat', sans-serif;
    font-size: 2.5vw;
    font-weight:700;
    color:#330CE9;
`;

const Testing = () => {
    return(
        <React.Fragment>
            <TestContainer>
                <TestTitle>Testing</TestTitle>
                <DescriptionTest>
                    <TestText>
                    Somos concientes de la calidad con la que debe contar cualquier producto
                    y en un sitio web mucho más, es por eso que hacemos lo necesario
                    para que tu sitio funcione a la perfeccion y sea un sitio seguro
                    para tus usuarios.<br/>
                    <SubTitle>¡Navega seguro y sin errores!</SubTitle>
                    </TestText>   
                </DescriptionTest> 
            </TestContainer>
        </React.Fragment>
    );
} 

export default Testing;