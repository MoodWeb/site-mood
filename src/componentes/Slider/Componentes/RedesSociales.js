import React from 'react';
import styled from 'styled-components';

const SocialMediaContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;
const SocialMediaTitle = styled.h1`
    font-size: 5vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #3498DB;
    text-align:center;
    color:#ffffff;
`;
const DescriptionSocialMedia = styled.div`
    width:100%;
    height:auto;
    margin:0;
    padding:0;
`;
const SocialMediaText = styled.p`
    width:50%;
    height:auto;
    margin:auto;
    font-family: 'Titillium Web', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: center;
    color:#424949;
    line-height:2.9vw;
`;
const SubTitle = styled.span`
    font-family: 'Montserrat', sans-serif;
    font-size: 2.5vw;
    font-weight:700;
    color:#330CE9;
`;
const RedesSociales = () => {
    return(
        <React.Fragment>
            <SocialMediaContainer>
                <SocialMediaTitle>Redes Sociales</SocialMediaTitle>
                <DescriptionSocialMedia>
                    <SocialMediaText>
                    Te ayudamos a que seas popular en los medios digitales,
                    a traves de las diferentes redes sociales,
                    haciendo que tus seguidores cada vez sean más!<br/>
                    <SubTitle>¡Todos te conoceran!</SubTitle>
                    </SocialMediaText>   
                </DescriptionSocialMedia>
            </SocialMediaContainer>
        </React.Fragment>
    );
} 

export default RedesSociales;