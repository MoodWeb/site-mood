import React from 'react';
import styled from 'styled-components';

const DesignContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;
const DesingTitle = styled.h1`
    font-size: 5vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #3498DB;
    text-align:center;
    color:#ffffff;
`;
const DescriptionDesign = styled.div`
    width:100%;
    height:auto;
    margin:0;
    padding:0;
`;
const DesignText = styled.p`
    width:50%;
    height:auto;
    margin:auto;
    font-family: 'Titillium Web', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: center;
    color:#424949;
    line-height:2.9vw;
`;
const SubTitle = styled.span`
    font-family: 'Montserrat', sans-serif;
    font-size: 2.5vw;
    font-weight:700;
    color:#330CE9;
`;
const Diseno = () => {
    return(
        <React.Fragment>
            <DesignContainer>
                <DesingTitle>Diseño y Programación Web</DesingTitle>
                <DescriptionDesign>
                    <DesignText>
                    Te ayudamos a crear tu sitio web funcional y vanguardista, 
                    que se adapte a todas las plataformas y fácil de administrar.<br/>
                    <SubTitle>¡Estarás a la vista de todos!</SubTitle>
                    </DesignText>
                    
                </DescriptionDesign>
            </DesignContainer>
        </React.Fragment>
    );
} 

export default Diseno;