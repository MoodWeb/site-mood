import React, { Component } from 'react';
import Slider from 'react-animated-slider';
import { Link } from 'react-router-dom';
import content from './content';
import 'react-animated-slider/build/horizontal.css';
import './AnimacionesSlider.css';
import './Styles.css';


class Slidr extends Component {
    render() { 
        return ( 
            <Slider className="slider-wrapper">
                {content.map((item, index) => (
                    <div
                        key={index}
                        className="slider-content"
                        style={{ background: `url('${item.image}') no-repeat center center` }}
                    >
                        <div className="inner">
                            <h1>{item.title}</h1>
                            <p>{item.description}</p>
                            <Link to={item.to}><button className="slider-button">{item.button}</button></Link>
                        </div>
                        <section>
                            <img src={item.userProfile} alt={item.user} />
                            <span>
                                Posted by <strong>{item.user}</strong>
                            </span>
                        </section>
                    </div>
                ))}
            </Slider>
         );
    }
}
 
export default Slidr;