import React, { Component} from 'react';
import styled from "styled-components";

const LoadContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
@media (max-width:375px){
    height:100%
}
`;
const InitLogo = styled.div`
    width:100%;
    margin:auto;
    background-color:#000;
@media (max-width:375px){
    height:100%
}
`;
const LogoLoading = styled.img`
    width:50%;
    margin:auto;
    margin-left:25%;
    margin-right:25%;
    animation: App-logo-spin infinite 20s linear;
    @keyframes App-logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
`;
class Splash extends Component{
    render(){
        return(
            <LoadContainer>
                <InitLogo>
                    <LogoLoading src="./img/MOOD_WEB.svg"/>
                </InitLogo>
            </LoadContainer>
        );
    }
}

export default Splash;