import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './componentes/Header';
import Footer from './componentes/Footer';
import AppPrincipal from './componentes/AppPrincipal';
import SectionA from './componentes/SectionA';
import SectionB from './componentes/SectionB';
import SectionC from './componentes/SectionC';
import SectionD from './componentes/SectionD';
import Diseno from './componentes/Slider/Componentes/Diseno';
import Hosting from './componentes/Slider/Componentes/Hosting';
import RedesSociales from './componentes/Slider/Componentes/RedesSociales';
import Testing from './componentes/Slider/Componentes/Testing';
import Splash from  './componentes/Splash';
import { ParallaxProvider } from 'react-scroll-parallax';

function App() {
  const [contenido, setContenido] = React.useState(<Splash />);
  const [cambio, setCambio] = React.useState(false);


    if(cambio === false){
      setCambio(true);
      setTimeout(() => {setContenido(
        <Router>
          <Header/>
            <Switch>
              <Route exact path="/" component={ AppPrincipal } />
              <Route exact path="/about" component={ SectionA } />
              <Route exact path="/team" component={ SectionB } />
              <Route exact path="/services" component={ SectionC } />
              <Route exact path="/contact" component={ SectionD }/>
              <Route exact path="/diseno" component={ Diseno }/>
              <Route exact path="/hosting" component={ Hosting }/>
              <Route exact path="/redessociales" component={ RedesSociales }/>
              <Route exact path="/testing" component={ Testing }/>
            </Switch>      
          <Footer/>
        </Router>
      )}, 5000);
    }
  return (
    <ParallaxProvider>
      { contenido }
    </ParallaxProvider>
  );
}

export default App;
